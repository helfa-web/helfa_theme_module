<?php

namespace Drupal\helfa_theme_module\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "helfa_theme_module_admin_navigation_button",
 *   admin_label = @Translation("Helfa Theme Module Admin Navigation Button Block"),
 * )
 */
class AdminNavigationButton extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'markup',
      '#markup' => '<i class="fa fa-lg fa-gear"></i>',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    //return AccessResult::allowedIfHasPermission($account, 'access printer-friendly version');
    /*
    var_dump($account->getRoles());
    if (in_array('helfa_member', $account->getRoles())) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
    //*/
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['helfa_admin_nav_block_settings'] = $form_state->getValue('helfa_admin_nav_block_settings');
  }
}
